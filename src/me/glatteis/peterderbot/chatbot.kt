package me.glatteis.peterderbot

import java.io.*
import java.util.*

/**
 * Created by Linus on 15.06.2016!
 */

open class Word(val word: String) : Serializable {
    companion object {
        private val serialVersionUID: Long = 13374206969
    }

    val followupWords = HashMap<String, Int>()
    open fun digestFollowupWord(word: Word) {
        followupWords.put(word.word, if (followupWords[word.word] == null) 1 else followupWords[word.word]!!.plus(1))
        word.use()
    }

    fun findFollowupWord(): String {
        var totalUses = 0
        followupWords.values.forEach { totalUses += it }
        var random = (Math.random() * totalUses).toInt()
        if (followupWords.keys.isEmpty()) return ";"
        var wordToReturn = followupWords.keys.first()
        for ((word, i) in followupWords) {
            random -= i
            if (random <= 0) {
                wordToReturn = word
                break
            }
        }
        return wordToReturn
    }

    fun containsWord(word: String) = followupWords.containsKey(word)

    var totalUsages = 0
    open fun use() {
        totalUsages++
    }
}

class Begin : Word("") {

    companion object {
        val serialVersionUID = 202639237309433968
    }

    init {
        totalUsages = Int.MAX_VALUE
        followupWords.remove(";")
    }

    override fun digestFollowupWord(word: Word) {
        if (word.word != ";") {
            super.digestFollowupWord(word)
        }
    }

    override fun use() {
    }
}

object End : Word(";")

class ChatBot(val words: ArrayList<Word>, var begin: Begin) {

    constructor() : this(ArrayList(), Begin())

    constructor(file: File) : this(ArrayList(), Begin()) {
        if (!file.exists()) return
        val inputStream = ObjectInputStream(FileInputStream(file))
        val obj = inputStream.readObject()
        if (obj !is ArrayList<*>) {
            println("Warning: File isn't valid.")
            return
        }
        for (element in obj) {
            if (element !is Word) {
                println("Warning: File isn't valid.")
                return
            }
            if (element is Begin) {
                begin = element
            }
            words.add(element)
        }
        inputStream.close()
    }

    fun saveToFile(file: File) {
        if (!words.contains(begin)) words.add(begin)
        val outputStream = ObjectOutputStream(FileOutputStream(file))
        outputStream.writeObject(words)
        outputStream.close()
    }

    /*
    Takes word as String and returns the Object.
    If words doesn't contain said word, this will create it and add it to the list.
     */
    fun findWord(word: String): Word {
        if (word == ";") return End
        words.forEach {
            if (it.word.equals(word)) return it
        }
        val wordObject = Word(word)
        words.add(wordObject)
        return wordObject
    }

    /*
    Returns given sentence in words, stripped
     */
    fun sentenceInWords(sentence: String): List<String> = sentence.toLowerCase().trim()
            .replace(Regex("[^a-zA-Z0-9öäüß& n@]"), "")
            .split(" ").filter { it.isNotBlank() }

    /*
    Saves sentence
     */
    fun digestSentence(sentence: String) {
        val sentenceInWords = sentenceInWords(sentence)
        if (sentenceInWords.size < 2) return
        begin.digestFollowupWord(findWord(sentenceInWords[0]))
        for (i in 0..sentenceInWords.size - 2) {
            findWord(sentenceInWords[i]).digestFollowupWord(findWord(sentenceInWords[i + 1]))
        }
        findWord(sentenceInWords.last()).digestFollowupWord(End)
    }

    /*
    Makes random sentence
     */
    fun makeSentence(charLimit: Int): String {
        var sentence = ""
        var currentWord = findWord(begin.findFollowupWord())
        sentence += currentWord.word
        while (true) {
            currentWord = findWord(currentWord.findFollowupWord())
            if (currentWord == End || (sentence + currentWord.word).length > charLimit) break
            sentence += " " + currentWord.word
        }
        return sentence
    }

    /*
    Tries to make sentence based on input
     */
    fun makeSentenceFromInput(charLimit: Int, inputSentence: String): String {
        digestSentence(inputSentence)
        val inputSentenceInWords = sentenceInWords(inputSentence)
        var sentence = ""
        var currentWord = findWord(begin.findFollowupWord())
        sentence += currentWord.word
        while (true) {
            var foundPreferredWord = false
            for (word in inputSentenceInWords) {
                if (currentWord.containsWord(word)) {
                    currentWord = findWord(word)
                    foundPreferredWord = Math.random() > 0.5
                    break
                }
            }
            if (!foundPreferredWord) currentWord = findWord(currentWord.findFollowupWord())
            if (currentWord == End || (sentence + currentWord.word).length > charLimit) break
            sentence += " " + currentWord.word
        }
        return sentence
    }

    /*
    Delete words that have the least usage
     */
    fun garbageCollect(maxWords: Int) {
        words.sortBy { it.totalUsages }
        while (words.size > maxWords) {
            val word = words[0]
            for (w in words) {
                w.followupWords.remove(word.word)
            }
            words.remove(word)
        }
    }

}