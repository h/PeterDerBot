package me.glatteis.peterderbot;

import twitter4j.Twitter;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.Authorization;

/**
 * Created by Linus on 15.06.2016!
 */
public class StreamInit {
    public static void initStream(Authorization auth, TwitterStreamFactory factory) {
        TwitterStream stream = factory.getInstance(auth);
        stream.addListener(new PeterStream());
        stream.user();
    }
}
