package me.glatteis.peterderbot

import twitter4j.*
import java.util.*

/**
 * Created by Linus on 15.06.2016!
 */

class PeterStream : UserStreamListener {
    override fun onUserListMemberAddition(p0: User?, p1: User?, p2: UserList?) {

    }

    override fun onFavorite(p0: User?, p1: User?, p2: Status?) {

    }

    override fun onBlock(p0: User?, p1: User?) {

    }

    override fun onUserListUpdate(p0: User?, p1: UserList?) {

    }

    override fun onUserListSubscription(p0: User?, p1: User?, p2: UserList?) {

    }

    override fun onQuotedTweet(p0: User?, p1: User?, p2: Status?) {

    }

    override fun onDeletionNotice(p0: Long, p1: Long) {

    }

    override fun onUserProfileUpdate(p0: User?) {

    }

    override fun onDirectMessage(message: DirectMessage) {
        if (message.text.startsWith("!") && message.senderId == 629278770L) {
            when (message.text) {
                "!save" -> PeterDerBot.chatBot.saveToFile(PeterDerBot.file)
                "!garbage" -> PeterDerBot.chatBot.garbageCollect(10000)
                "!tweet" -> PeterDerBot.twitter.updateStatus(PeterDerBot.chatBot.makeSentence(140))
            }
            if (message.text.startsWith("!trim")) {
                val words = message.text.split(" ")
                if (words.size != 2) return
                val number = words[1].toInt()
                PeterDerBot.chatBot.garbageCollect(number)
            }
        }
    }

    override fun onUserListUnsubscription(p0: User?, p1: User?, p2: UserList?) {

    }

    override fun onFollow(user1: User, user2: User) {
        if (user2.id == PeterDerBot.twitter.id) {
            PeterDerBot.twitter.createFriendship(user1.id)
        }
    }

    override fun onUserListMemberDeletion(p0: User?, p1: User?, p2: UserList?) {

    }

    override fun onUserListDeletion(p0: User?, p1: UserList?) {

    }

    override fun onUnfollow(p0: User?, p1: User?) {

    }

    override fun onRetweetedRetweet(p0: User?, p1: User?, p2: Status?) {

    }

    override fun onUserListCreation(p0: User?, p1: UserList?) {

    }

    override fun onFavoritedRetweet(p0: User?, p1: User?, p2: Status?) {

    }

    override fun onUnfavorite(p0: User?, p1: User?, p2: Status?) {

    }

    override fun onUserDeletion(p0: Long) {

    }

    override fun onFriendList(p0: LongArray?) {

    }

    override fun onUnblock(p0: User?, p1: User?) {

    }

    override fun onUserSuspension(p0: Long) {

    }

    override fun onScrubGeo(p0: Long, p1: Long) {

    }

    override fun onDeletionNotice(p0: StatusDeletionNotice?) {

    }

    override fun onTrackLimitationNotice(p0: Int) {

    }

    override fun onStallWarning(p0: StallWarning?) {

    }

    override fun onStatus(status: Status) {
        if (status.user.id == PeterDerBot.twitter.id || status.isRetweet) {
            return
        }
        val sentence = ArrayList(status.text.split(" "))
        sentence.filter { it.startsWith("@") && it != "@peterderbot" }.forEach {
            val username = it.removePrefix("@")
            println(username)
            val user = PeterDerBot.twitter.showUser(username)
            if (user.lang == "de" &&
                    !PeterDerBot.twitter.showFriendship(PeterDerBot.twitter.screenName, username).isSourceFollowingTarget) {
                PeterDerBot.twitter.createFriendship(username)
                println("Followed $username.")
            }
        }
        sentence.removeIf { it.startsWith("@") || it.startsWith("http") }
        val text = sentence.joinToString(" ")
        if (status.userMentionEntities.any { it.id == PeterDerBot.twitter.id } || status.inReplyToUserId == PeterDerBot.twitter.id) {
            println("Mention by " + status.user.screenName + ": " + text)
            var mentionText = ""
            status.userMentionEntities.forEach {
                if (it.id != PeterDerBot.twitter.id) {
                    mentionText += "@" + it.screenName + " "
                }

            }
            mentionText += "@" + status.user.screenName + " "
            // This used to be of length 140 - mentionText.length, but Twitter has changed this since.
            val newText = mentionText + PeterDerBot.chatBot.makeSentenceFromInput(140, text)
            val newStatus = StatusUpdate(newText)
            newStatus.inReplyToStatusId = status.id
            PeterDerBot.twitter.updateStatus(newStatus)
            println("Replied " + newText)
        } else {
            PeterDerBot.chatBot.digestSentence(text)
        }
    }

    override fun onException(p0: Exception?) {

    }

}