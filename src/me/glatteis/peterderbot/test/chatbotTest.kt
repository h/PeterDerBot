package me.glatteis.peterderbot.test

import me.glatteis.peterderbot.ChatBot
import java.io.File
import java.util.*

/**
 * Created by Linus on 15.06.2016!
 */

object A

fun main(args: Array<String>) {
    val chatbot = ChatBot()
    val file = File("memes.data")
    val sentences = A.javaClass.getResource("testSentences.txt").readText().split("\n")
    sentences.forEach {
        chatbot.digestSentence(it)
    }
    chatbot.saveToFile(file)
    println(file.length())
    val newChatbot = ChatBot(file)
    newChatbot.garbageCollect(10)
    newChatbot.saveToFile(file)
    println(file.length())
}